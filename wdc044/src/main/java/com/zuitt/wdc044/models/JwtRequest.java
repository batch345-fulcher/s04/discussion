package com.zuitt.wdc044.models;

import java.io.Serializable;
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 1730993934201187817L;

    // Properties
    private String username;
    private String password;

    // Constructors
    public JwtRequest(){}

    public JwtRequest(String username, String password){
        this.setUsername(username);
        this.setPassword(password);
    }

    // Getters and Setters
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public  void setPassword(String password){
        this.password = password;
    }
}
